// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SetFullScreen(){
	window_set_fullscreen(true);

	if(!window_get_fullscreen()){
		screenWidth = 240;
		screenHeight = 135;

		///set window size
		window_set_size(screenWidth, screenHeight);
		obj_game.fullscreen = false;
	}else{
		screenWidth = display_get_width();
		screenHeight = display_get_height();
		obj_game.fullscreen = true;
	}
}