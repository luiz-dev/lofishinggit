// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ScreenShake(){
	with (obj_camera)
	{	
		shake_amp = argument0;
		shake_degrade = argument0/argument1;
		shake_dir = random(360);
	}
}