/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
spd = choose (-2.2, 2.2);
value = -100;
isGhost = true;

//Generated for GMS2 in Geon FX v1.3
//Put this code in Create event

//NewEffect Particle System
ps = part_system_create();
part_system_depth(ps, -1);

//NewEffect Particle Types
//Effect1
pt_Effect1 = part_type_create();
part_type_shape(pt_Effect1, pt_shape_cloud);
part_type_size(pt_Effect1, 0.07, 0.09, 0, 0);
part_type_scale(pt_Effect1, 1, 1);
part_type_orientation(pt_Effect1, 0, 0, 0, 0, 0);
part_type_color3(pt_Effect1, 255, 4194432, 12615935);
part_type_alpha3(pt_Effect1, 1, 1, 1);
part_type_blend(pt_Effect1, 0);
part_type_life(pt_Effect1, 40, 40);
part_type_speed(pt_Effect1, .5, 1, 0, 0);
part_type_direction(pt_Effect1, 50, 125, 0, 0);
part_type_gravity(pt_Effect1, 0, 0);

//NewEffect Emitters
pe_Effect1 = part_emitter_create(ps);