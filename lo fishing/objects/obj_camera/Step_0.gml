/// @description Insert description here
// You can write your code in this editor
if(instance_exists(obj_anzol)){
	camera_ypos = clamp(obj_anzol.y-70, 0, room_height);
	
	timer++;

	//Screen shake
	x += lengthdir_x(shake_amp,shake_dir);
	camera_ypos += lengthdir_y(shake_amp,shake_dir);
	if(timer mod 1 == 0)
	{
		shake_dir += 180;
		shake_amp = Approach(shake_amp,0,shake_degrade);
		
		if(shake_amp == 0){
			x = 0;
		}
	}
	camera_ypos = clamp(camera_ypos, 0, room_height-135)
	camera_set_view_pos(camera, x, camera_ypos);
}