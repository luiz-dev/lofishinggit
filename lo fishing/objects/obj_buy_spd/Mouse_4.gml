/// @description Insert description here
// You can write your code in this editor

if(can_click){
	if (obj_game.money >= 100){
		with(obj_game){
			money-=100;
		}
		with(obj_anzol){
			spd+=1;
		}
		audio_play_sound(lofishing_sfx_buy, 1, 0);
		var temp = instance_create_layer(x,y,layer,obj_text_pop);
		temp.text = "-100";
		temp.depth = depth-1;
		temp.color = c_red;
		with(obj_close){
			instance_destroy();
		}
		
		with(obj_button){
			instance_destroy();
		}
		obj_shop.visible = true;
		instance_destroy();
	}
}