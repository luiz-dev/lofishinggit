/// @description Insert description here
// You can write your code in this editor

switch(state){
	case 0:
		if(keyboard_check(vk_down)){
			y+=spd;
			
		}

		if(keyboard_check(vk_up)){
			y-=spd;
		}

		if(keyboard_check(vk_right)){
			x+=spd;
		}

		if(keyboard_check(vk_left)){
			x-=spd;
		}
		
		//y_starter+obj_game.meters
		
		//max_y = clamp(max_y, 
		var max_y = y_starter+obj_game.meters
		//if(obj_game.meters > room_height-30){
		//	max_y = room_height-30;
		//}
		
		
		y = clamp(y,y_starter, max_y);
		x = clamp(x, 4, room_width - 4);
		if(y-y_starter == obj_game.meters){
			state = 1;
		}
	break;
	case 1:
		y-=spd*3;
		if(y<=0){
			state = 0;
			with(fish_pick){
				instance_destroy();
			}
			with(obj_game){
				event_user(0);
			}
			fish_pick = noone;
		}
}