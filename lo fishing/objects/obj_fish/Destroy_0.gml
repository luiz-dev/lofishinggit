/// @description Insert description here
// You can write your code in this editor
obj_game.money+=value;

var temp = instance_create_layer(x,y,layer, obj_text_pop);
temp.depth = depth-1;
temp.text = string(value);
if(isGhost){
	temp.color = c_red;
	audio_play_sound(lofishing_sfx_fish_bad, 1, 0);
}else{
	audio_play_sound(lofishing_sfx_fish_good, 1, 0);
}