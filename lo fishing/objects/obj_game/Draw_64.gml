/// @description Insert description here
// You can write your code in this editor
if(room == rm_game){
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	if(obj_shop.visible){
		draw_set_font(Font1);
		draw_set_color(c_black);
		draw_text(32-1, 16+1, "coins: "+string(money));
		draw_set_color(c_white);
		draw_text(32, 16, "coins: "+string(money));
		if(instance_exists(obj_anzol)){
			draw_set_color(c_black);
			draw_text(room_width/2-1, 16+1, string(meters-(obj_anzol.y-obj_anzol.y_starter))+"M");
			draw_set_color(c_white);
			draw_text(room_width/2, 16, string(meters-(obj_anzol.y-obj_anzol.y_starter))+"M");
		}
	}
}