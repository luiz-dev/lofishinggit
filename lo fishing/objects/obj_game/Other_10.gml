/// @description Insert description here
// You can write your code in this editor
total_fish = instance_number(obj_fish);

for(var i = 0; i < instance_number(obj_fish);i++){
	var temp = instance_find(obj_fish, i);
	if(temp.isGhost){
		total_fish -= 1;
	}
}

if(total_fish == 0){
	room_goto(rm_menu);
}