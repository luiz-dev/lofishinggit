/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

//NewEffect emitter positions. Streaming or Bursting Particles.
var xp, yp;
xp = x;
yp = y;
part_emitter_region(ps, pe_Effect1, xp-15, xp+15, yp-1, yp+1, ps_shape_rectangle, ps_distr_linear);
part_emitter_stream(ps, pe_Effect1, pt_Effect1, 1);