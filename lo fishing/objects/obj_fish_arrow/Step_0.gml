/// @description Insert description here
// You can write your code in this editor
if(keyboard_check_pressed(vk_down)){
	pos+=1;
	if(pos > 1){
		pos = 0;
	}
}

if(keyboard_check_pressed(vk_up)){
	pos-=1;
	if(pos < 0){
		pos = 1;
	}
}

if(pos == 0){
	x = 156;
	y = 100;
}else{
	x = 156;
	y = 120;
}

if(keyboard_check_pressed(vk_enter) or keyboard_check_pressed(vk_return) and sprite_index != shine){
	image_index = 0;
	sprite_index = shine;
	audio_play_sound(lofishing_sfx_click, 1, 0);
}