/// @desc Size variables and mode setup

enum TRANS_MODE
{
	OFF,
	NEXT,
	GOTO,
	RESTART,
	INTRO
}

w = obj_camera.view_width;
h = obj_camera.view_height;
h_half = h * 0.5;

color_fade = c_black;

mode = TRANS_MODE.INTRO;
percent = 1;
target = room;

depth = -100;