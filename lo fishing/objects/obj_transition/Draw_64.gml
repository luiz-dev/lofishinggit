/// @desc Draw black bars

if (mode != TRANS_MODE.OFF)
{
	draw_set_color(color_fade);
	draw_set_alpha(percent);
	draw_rectangle_color(-2,0,w,h, c_black, c_black, c_black, c_black, 0);
	draw_set_alpha(1);
}else{
	draw_set_alpha(1);
}
